const User = require("../models/User.js");
const Product = require("../models/Product.js");
const Order = require("../models/Order.js");

// Controller route to place an order
module.exports.addOrder = (reqBody, userData) => {

	return User.findById(userData.userId).then((result) => {
		if(userData.isAdmin) {
			return 'Only Users are allowed to order!'
		} else {
			let newOrder = new Order({
				userId: userData.userId,
				productId: reqBody.productId,
				productName: reqBody.productName,
				productPrice: reqBody.productPrice,
				quantity: reqBody.quantity,
				totalAmount: reqBody.quantity * reqBody.productPrice
			})
			return newOrder.save().then((Order, error) => {
				if(error) {
					return false;
				} else {
					return 'Order successfully added!'
				}
			})
		}
	})
};


// Stretch Goals: Controller Route to retrieve all orders
module.exports.allOrders = (data) => {

	if(data.isAdmin) {
		return Order.find({}).then(result => {
			return result
		})
	} else {
		return false
	}
};