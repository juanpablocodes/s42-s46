const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');

// Capstone Making Day 2 Deliverable:

// Create a Product Sample Workflow:
// 1. An authenticated admin user sends a POST request containing a JWT in its header to the /products endpoint.
// 2. API validates JWT, returns false if validation fails.
// 3. If validation successful, API creates product using the contents of the request body.

// Retrieve All Active Products Sample Workflow:
// 1. A GET request is sent to the /products endpoint.
// 2. API retrieves all active products and returns them in its response.


// Route for creating a product
router.post("/", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	productController.addProduct(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => {
		res.send(resultFromController);
	})
});

// Route for retrieving all products
// router.get('/', auth.verify, (req, res) => {
	
// 	const userData = auth.decode(req.headers.authorization);

// 	productController.getAllProducts(userData).then(resultFromController => {
// 		res.send(resultFromController)
// 	})
// });

// Route to retrieve all active products
router.get('/', (req, res) => {
	productController.getAllActive().then(resultFromController => {
		res.send(resultFromController)
	})
});

// Route to retrieve specific product
router.get('/:productId', (req, res) => {

	console.log(req.params.productId)

	productController.getSpecificProduct(req.params).then(resultFromController => {
		res.send(resultFromController)
	})
});



// Route to update a product
router.put('/:productId', auth.verify, (req, res) => {

	const isAdminData = auth.decode(req.headers.authorization).isAdmin;

	productController.updateProduct(req.params, req.body, isAdminData).then(resultFromController => {
		res.send(resultFromController);
	})
});

// Route to archive a product
router.put('/:productId/archive', auth.verify, (req, res)=> {
	const data = {
		productId: req.params.productId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.archiveProduct(data).then(resultFromController => {
		res.send(resultFromController)
	})
});


module.exports = router;