const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

// Route to registration
router.post ("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route to user authentication
router.post ("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
}); 

// Route to retrieve User details
router.get('/details', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	console.log(userData)
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});

router.put("/updateAdmin", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	userController.setUserToAdmin(req.body, userData).then(resultFromController => res.send(resultFromController))
});

module.exports = router;